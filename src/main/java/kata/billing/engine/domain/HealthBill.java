package kata.billing.engine.domain;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HealthBill {

  private final Map<HealthService, List<HealthProduct>> healthServicesProvided;

  private HealthBill(HealthService healthService) {
    healthServicesProvided = new HashMap<>();
    add(healthService);
  }

  public static HealthBillBuilder of(HealthService healthService) {
    return new HealthBillBuilder(new HealthBill(healthService));
  }

  public void add(HealthService healthService) {
    healthServicesProvided.put(healthService, Collections.<HealthProduct>emptyList());
  }

  public void add(HealthService healthService, List<HealthProduct> products) {
    healthServicesProvided.put(healthService, products);
  }

  public double chargeForProducts() {
    return healthServicesProvided.values().stream().flatMap(List::stream)
        .collect(Collectors.toList()).stream().mapToDouble(HealthProduct::getPrice).sum();
  }

  public double chargeForServices() {
    return healthServicesProvided.keySet().stream().mapToDouble(HealthService::getPrice).sum();
  }

  public boolean contains(HealthService service) {
    return healthServicesProvided.containsKey(service);
  }
}
