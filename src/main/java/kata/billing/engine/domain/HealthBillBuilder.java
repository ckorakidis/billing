package kata.billing.engine.domain;

import java.util.List;

public class HealthBillBuilder {

  private final HealthBill healthBill;

  public HealthBillBuilder(HealthBill healthBill) {
    this.healthBill = healthBill;
  }

  public HealthBillBuilder and(HealthService healthService) {
    healthBill.add(healthService);
    return this;
  }

  public HealthBillBuilder and(HealthService healthService, List<HealthProduct> productsConsumed) {
    healthBill.add(healthService, productsConsumed);
    return this;
  }

  public HealthBill create() {
    return healthBill;
  }

}
