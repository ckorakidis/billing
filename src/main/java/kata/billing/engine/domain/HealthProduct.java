package kata.billing.engine.domain;

public enum HealthProduct {
  VACCINE(15.0);

  private final double price;

  private HealthProduct(double price) {
    this.price = price;
  }

  public double getPrice() {
    return price;
  }
}
