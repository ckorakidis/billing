package kata.billing.engine.domain;

public enum HealthService {
  DIAGNOSIS(60), XRAY(150), BLOD_TEST(78), EGG(200.4),  VACCINE(27.5);

  private final double price;

  private HealthService(double price) {
    this.price = price;
  }

  public double getPrice() {
    return price;
  }
}
