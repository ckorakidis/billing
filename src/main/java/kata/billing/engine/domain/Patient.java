package kata.billing.engine.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Patient {

  private final int age;
  private final boolean insuredByMediHealth;
}
