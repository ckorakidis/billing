package kata.billing.engine.service;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BiFunction;

import kata.billing.engine.domain.HealthBill;
import kata.billing.engine.domain.Patient;
import lombok.NonNull;

public class BillingEngine {

  private Set<BiFunction<HealthBill, Patient, Double>> discountPolicies;

  private BillingEngine() {
    discountPolicies = new HashSet<>();
  }

  public static BillingEngineBuilder with(@NonNull BiFunction<HealthBill, Patient, Double> policy) {
    BillingEngine engine = new BillingEngine();
    engine.addDiscountPolicy(policy);
    return new BillingEngineBuilder(engine);
  }

  public void addDiscountPolicy(@NonNull BiFunction<HealthBill, Patient, Double> policy) {
    discountPolicies.add(policy);
  }

  public double finalAmountToCharge(@NonNull HealthBill bill, @NonNull Patient patient) {
    return bill.chargeForServices() - reducedChargeForServices(bill, patient) + bill
        .chargeForProducts();
  }

  private double reducedChargeForServices(HealthBill bill, Patient patient) {
    return discountPolicies.stream().mapToDouble(policy -> policy.apply(bill, patient)).sum();
  }

}
