package kata.billing.engine.service;

import java.util.function.BiFunction;

import kata.billing.engine.domain.HealthBill;
import kata.billing.engine.domain.Patient;
import lombok.NonNull;

public class BillingEngineBuilder {

  private final BillingEngine engine;

  public BillingEngineBuilder(@NonNull BillingEngine engine) {
    this.engine = engine;
  }

  public BillingEngineBuilder and(@NonNull BiFunction<HealthBill, Patient, Double> policy) {
    engine.addDiscountPolicy(policy);
    return this;
  }

  public BillingEngine create() {
    return engine;
  }
}
