package kata.billing.engine.service.discount;

import java.util.function.BiFunction;

import kata.billing.engine.domain.HealthBill;
import kata.billing.engine.domain.Patient;
import lombok.NonNull;

public abstract class BasicDiscountPolicy implements BiFunction<HealthBill, Patient, Double> {

  @Override
  public Double apply(@NonNull HealthBill healthBill, @NonNull Patient patient) {
    if ((patient.getAge() < minAgePolicy()) || (patient.getAge() > maxAgePolicy())) {
      return 0.0;
    }
    return healthBill.chargeForServices() * basicDiscount();
  }

  protected abstract int minAgePolicy();

  protected abstract int maxAgePolicy();

  protected abstract double basicDiscount();
}
