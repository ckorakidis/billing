package kata.billing.engine.service.discount;

import java.util.function.BiFunction;

import kata.billing.engine.domain.HealthBill;
import kata.billing.engine.domain.HealthService;
import kata.billing.engine.domain.Patient;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@EqualsAndHashCode
public class BloodDiscountPolicy implements BiFunction<HealthBill, Patient, Double> {

  private final double BLOOD_TEST_DISCOUNT = 0.15;

  @Override
  public Double apply(@NonNull HealthBill healthBill, @NonNull Patient patient) {

    if ((patient.isInsuredByMediHealth()) &&
        healthBill.contains(HealthService.BLOD_TEST) &&
        healthBill.contains(HealthService.DIAGNOSIS)) {
      return HealthService.BLOD_TEST.getPrice() * BLOOD_TEST_DISCOUNT;
    }

    return 0.0;
  }

}
