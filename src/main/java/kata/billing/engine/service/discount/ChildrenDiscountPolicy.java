package kata.billing.engine.service.discount;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class ChildrenDiscountPolicy extends BasicDiscountPolicy {

  public static final int MIN_AGE = 0;

  @Override
  protected int minAgePolicy() {
    return MIN_AGE;
  }

  @Override
  protected int maxAgePolicy() {
    return 5;
  }

  @Override
  protected double basicDiscount() {
    return 0.4;
  }
}
