package kata.billing.engine.service.discount;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class OlderSeniorDiscountPolicy extends BasicDiscountPolicy {

  public static final int MAX_AGE = 150;

  @Override
  protected int minAgePolicy() {
    return 75;
  }

  @Override
  protected int maxAgePolicy() {
    return MAX_AGE;
  }

  @Override
  protected double basicDiscount() {
    return 0.9;
  }
}
