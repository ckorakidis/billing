package kata.billing.engine.service.discount;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class SeniorDiscountPolicy extends BasicDiscountPolicy {

  @Override
  protected int minAgePolicy() {
    return 65;
  }

  @Override
  protected int maxAgePolicy() {
    return 70;
  }

  @Override
  protected double basicDiscount() {
    return 0.6;
  }
}
