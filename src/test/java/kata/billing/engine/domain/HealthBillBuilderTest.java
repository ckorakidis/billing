package kata.billing.engine.domain;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HealthBillBuilderTest {

  @Test
  public void and_addsTheGivenHealthService_toTheCreatedBill() {
    HealthBill bill = HealthBill.of(HealthService.BLOD_TEST).and(HealthService.DIAGNOSIS).create();
    assertTrue(bill.contains(HealthService.DIAGNOSIS));
  }

  @Test
  public void and_addsTheGivenHealthServiceAndProducts_toTheCreatedBill() {
    HealthBill bill = HealthBill
        .of(HealthService.BLOD_TEST)
        .and(HealthService.VACCINE, Arrays.asList(HealthProduct.VACCINE, HealthProduct.VACCINE))
        .create();

    assertEquals(105.5, bill.chargeForServices(), 0.01);
    assertEquals(30, bill.chargeForProducts(), 0.01);
  }
}