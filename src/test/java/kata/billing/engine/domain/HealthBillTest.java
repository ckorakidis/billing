package kata.billing.engine.domain;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HealthBillTest {

  @Test
  public void of_addsTheGivenServiceToTheBill_whenCreated() throws Exception {

    assertTrue(HealthBill.of(HealthService.DIAGNOSIS).create().contains(HealthService.DIAGNOSIS));
  }

  @Test
  public void add_addsTheGivenServiceToTheBill_whenCreated() throws Exception {

    HealthBill bill = HealthBill
        .of(HealthService.DIAGNOSIS)
        .and(HealthService.EGG)
        .create();

    assertTrue(bill.contains(HealthService.DIAGNOSIS));
    assertTrue(bill.contains(HealthService.EGG));
  }

  @Test
  public void chargeForProducts_calculatesTheCharge_forTheGivenProducts() throws Exception {
    HealthBill bill = HealthBill.of(HealthService.DIAGNOSIS).create();
    bill.add(HealthService.VACCINE, Arrays.asList(HealthProduct.VACCINE, HealthProduct.VACCINE));

    assertEquals(30, bill.chargeForProducts(), 0.01);
  }

  @Test
  public void chargeForServices_calculatesTheCharge_forTheGivenServices() throws Exception {
    HealthBill bill = HealthBill
        .of(HealthService.DIAGNOSIS)
        .and(HealthService.EGG)
        .create();

    assertEquals(260.4, bill.chargeForServices(), 0.01);
  }
}