package kata.billing.engine.service;

import org.junit.Before;
import org.junit.Test;

import kata.billing.engine.domain.HealthBill;
import kata.billing.engine.domain.HealthService;
import kata.billing.engine.domain.Patient;
import kata.billing.engine.service.discount.OlderSeniorDiscountPolicy;
import kata.billing.engine.service.discount.SeniorDiscountPolicy;

import static org.junit.Assert.assertEquals;

public class BillingEngineBuilderTest {

  private BillingEngine engine;

  private HealthBill bill;

  @Before
  public void setUp() {
    engine = BillingEngine
        .with(new SeniorDiscountPolicy())
        .and(new OlderSeniorDiscountPolicy())
        .create();

    bill = HealthBill.of(HealthService.XRAY).create();
  }

  @Test
  public void and_addsTheGivenPolicy_toTheGivenEngine() {

    assertEquals(15, engine.finalAmountToCharge(bill, new Patient(80, false)), 0.01);
  }

  @Test(expected = NullPointerException.class)
  public void and_throwsException_wenNoBillIsGiven() {

    engine.finalAmountToCharge(null, new Patient(80, false));
  }

  @Test(expected = NullPointerException.class)
  public void and_throwsException_wenNoPatientIsGiven() {

    engine.finalAmountToCharge(bill, null);
  }
}