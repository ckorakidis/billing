package kata.billing.engine.service.discount;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import kata.billing.engine.domain.HealthBill;
import kata.billing.engine.domain.HealthProduct;
import kata.billing.engine.domain.HealthService;
import kata.billing.engine.domain.Patient;
import kata.billing.engine.service.BillingEngine;

import static org.junit.Assert.assertEquals;

public class BillingEngineSpec {

  private BillingEngine billingEngine;

  private HealthBill allServicesBill;

  @Before
  public void setUp() {
    billingEngine = BillingEngine
        .with(new SeniorDiscountPolicy())
        .and(new OlderSeniorDiscountPolicy())
        .and(new ChildrenDiscountPolicy())
        .and(new BloodDiscountPolicy())
        .create();

    allServicesBill = HealthBill
        .of(HealthService.DIAGNOSIS)
        .and(HealthService.XRAY)
        .and(HealthService.BLOD_TEST)
        .and(HealthService.EGG)
        .and(HealthService.VACCINE, Arrays.asList(HealthProduct.VACCINE, HealthProduct.VACCINE))
        .create();
  }

  @Test
  public void offersBasicAgeDiscount_whenCharging_notInsuredSeniors() {
    double amountToPay = billingEngine.finalAmountToCharge(allServicesBill, new Patient(67, false));

    assertEquals(236.36, amountToPay, 0.01);
  }

  @Test
  public void offersBasicAgeDiscount_whenCharging_notInsuredOlderSeniors() {
    double amountToPay = billingEngine.finalAmountToCharge(allServicesBill, new Patient(80, false));

    assertEquals(81.59, amountToPay, 0.01);
  }

  @Test
  public void offersBasicAgeDiscount_whenCharging_notInsuredChildren() {
    double amountToPay = billingEngine.finalAmountToCharge(allServicesBill, new Patient(4, false));

    assertEquals(339.54, amountToPay, 0.01);
  }

  @Test
  public void offersBloodDiscount_whenCharging_insuredDiagnosedSeniors() {
    HealthBill bill = HealthBill
        .of(HealthService.DIAGNOSIS)
        .and(HealthService.BLOD_TEST)
        .create();

    double amountToPay = billingEngine.finalAmountToCharge(bill, new Patient(67, true));

    assertEquals(43.5, amountToPay, 0.01);
  }

  @Test(expected = NullPointerException.class)
  public void throwsException_whenNoBillGiven() {
    billingEngine.finalAmountToCharge(null, new Patient(1, false));
  }

  @Test(expected = NullPointerException.class)
  public void throwsException_whenNoPatientGiven() {
    billingEngine.finalAmountToCharge(allServicesBill, null);
  }
}