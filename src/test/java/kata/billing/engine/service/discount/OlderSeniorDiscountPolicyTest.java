package kata.billing.engine.service.discount;

import org.junit.Before;
import org.junit.Test;

import java.awt.*;

import kata.billing.engine.domain.HealthBill;
import kata.billing.engine.domain.HealthService;
import kata.billing.engine.domain.Patient;

import static org.junit.Assert.*;

public class OlderSeniorDiscountPolicyTest {
  private OlderSeniorDiscountPolicy policy;
  private HealthBill bill;

  @Before
  public void setUp() {
    policy = new OlderSeniorDiscountPolicy();
    bill = HealthBill.of(HealthService.DIAGNOSIS).create();
  }

  @Test
  public void applyOlderSeniorDiscount_calculatesTheAmountToBeDeducted_fromProvidedServices() {
    assertEquals(54, policy.apply(bill, new Patient(80, false)), 0.01);
  }

  @Test
  public void applyOlderSeniorDiscount_doesNotApplyDiscountForNonOlderSenior_fromProvidedServices() {

    assertEquals(0, policy.apply(bill, new Patient(24, false)), 0.01);
  }

  @Test(expected = NullPointerException.class)
  public void applyDiscount_forNonExistingPatient_throwsException() {

    policy.apply(bill, null);
  }

  @Test(expected = NullPointerException.class)
  public void applyDiscount_forNonExistingBill_throwsException() {

    policy.apply(null, new Patient(24, false));
  }
}