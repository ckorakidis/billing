package kata.billing.engine.service.discount;

import org.junit.Before;
import org.junit.Test;

import kata.billing.engine.domain.HealthBill;
import kata.billing.engine.domain.HealthService;
import kata.billing.engine.domain.Patient;

import static org.junit.Assert.assertEquals;

public class SeniorDiscountPolicyTest {

  private SeniorDiscountPolicy policy;
  private HealthBill bill;

  @Before
  public void setUp() {
    policy = new SeniorDiscountPolicy();
    bill = HealthBill.of(HealthService.DIAGNOSIS).create();
  }

  @Test
  public void applySeniorDiscount_calculatesTheAmountToBeDeducted_fromProvidedServices() {

    assertEquals(36, policy.apply(bill, new Patient(70, false)), 0.01);
  }

  @Test
  public void applySeniorDiscount_doesNotApplyDiscountForNonSenior_fromProvidedServices() {

    assertEquals(0, policy.apply(bill, new Patient(24, false)), 0.01);
  }

  @Test(expected = NullPointerException.class)
  public void applyDiscount_forNonExistingPatient_throwsException() {

    policy.apply(bill, null);
  }

  @Test(expected = NullPointerException.class)
  public void applyDiscount_forNonExistingBill_throwsException() {

    policy.apply(null, new Patient(24, false));
  }
}